# WekaMongo

Maven JavaFx application to management mongoDb with Weka methods. 
Includes exporting/importing ARFF and CSF files to/from mongo database.

## Dependency

```
mongoDb 3.4.1
weka 3.6.6
```