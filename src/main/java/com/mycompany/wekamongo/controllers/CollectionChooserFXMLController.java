/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wekamongo.controllers;

import com.mongodb.client.MongoIterable;
import com.mycompany.wekamongo.database.MongoContext;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Kamil
 */
public class CollectionChooserFXMLController implements Initializable {
    
    @FXML
    private ComboBox collectionComboBox;

    
    private MongoContext db;
    private String name;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        db = MongoContext.getInstance(null);
        MongoIterable<String> names = db.getCollectionNames();
        if(names != null){
            for(String name : names){
                collectionComboBox.getItems().add(name);
            }
            collectionComboBox.getSelectionModel().selectFirst();
        }
    }    
    
    
    
    @FXML
    private void loadCollectionAction(ActionEvent event){
        name = (String)collectionComboBox.getValue();
        if(name!=null && !name.isEmpty()){
            ((Stage)collectionComboBox.getScene().getWindow()).close();
        }
    }
    
    public String getCollectionName(){
        return name;
    }
    
}
