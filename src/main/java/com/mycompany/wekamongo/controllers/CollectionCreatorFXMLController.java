/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wekamongo.controllers;

import com.mycompany.wekamongo.database.MongoContext;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Kamil
 */
public class CollectionCreatorFXMLController implements Initializable {

    @FXML
    private Button createButton;
    @FXML
    private TextField nameCollectionText;
    @FXML
    private Label errorCollectionLabel;
    
    private String name;
    
    MongoContext db;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        db = MongoContext.getInstance(null);
    }    
    
    @FXML
    private void createButtonAction(ActionEvent event){
        String value = nameCollectionText.getText();
        if(value != null && !value.isEmpty()){
            if(!db.collectionExists(value)){
                db.createCollection(value);
                name = value;
                ((Stage)createButton.getScene().getWindow()).close();
            } else {
                errorCollectionLabel.setVisible(true);
            }

        }
        
    }
    
    public String getNameCollection(){
        return name;
    }
    
    
}
