/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wekamongo.controllers;

import com.mycompany.wekamongo.database.MongoContext;
import com.mycompany.wekamongo.helpers.FileManagement;
import com.mycompany.wekamongo.helpers.WekaWrapper;
import com.mycompany.wekamongo.models.FileDataModel;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleButton;
import javafx.stage.Stage;
import org.bson.Document;
import weka.core.Instance;
import weka.core.Instances;

/**
 * FXML Controller class
 *
 * @author Kamil
 */
public class ExportCollectionFXMLController implements Initializable {

    @FXML
    private Button saveButton;
    @FXML
    private Label nameCollectionLabel;
    @FXML
    private ToggleButton loadButton;
    @FXML
    private TableView<Instance> contentTableView;
    private TableColumn<Instance,String>[] attributes;
    private ObservableList<Instance> instanceRows;
//    @FXML
    
    
    
    private MongoContext db;
    private FileDataModel loadedData;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        db = MongoContext.getInstance(null);
    }
    
    public void setNameCollection(String name){
        nameCollectionLabel.setText(name);
    }
    
    
    @FXML
    private void loadButtonAction(ActionEvent event){
        if(loadButton.isSelected()){
            loadedData = FileManagement.loadFromFile();
            if(loadedData!=null){
                loadButton.setText(loadedData.getFilename());
                showInTable(loadedData.getInstances());
            }
        } else {
            loadedData = null;
            contentTableView.setItems(null);
            contentTableView.getColumns().clear();
            loadButton.setText("Wczytaj");
        }
    }
    
    @FXML
    private void saveButtonAction(ActionEvent event){
        if(loadedData!=null){
            System.out.println("saving...");
            List<Document> documents = WekaWrapper.convertToDocuments(loadedData.getInstances());
            db.insertDocuments(documents);
            ((Stage)saveButton.getScene().getWindow()).close();
        }
    }
    
    
    //TABLE
    private void showInTable(Instances data){
        createTable(data);
        setItemsToTable(data);
    }
    
    private void createTable(Instances data){
        attributes = new TableColumn[data.numAttributes()];
        for(int i=0;i<attributes.length;i++){
            final int j = i;
            attributes[i] = new TableColumn<Instance,String>(data.attribute(i).name());
            attributes[i].setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().toString(j)));
        }
        contentTableView.getColumns().addAll(attributes);
    }
    
    private void setItemsToTable(Instances data){
        ArrayList<Instance> list = new ArrayList<Instance>();
        for(int i=0;i<data.numInstances();i++){
            list.add(data.instance(i));
        }
        contentTableView.setItems(FXCollections.observableList(list));
    }
}
