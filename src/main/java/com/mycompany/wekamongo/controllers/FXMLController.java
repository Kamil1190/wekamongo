package com.mycompany.wekamongo.controllers;

import com.mongodb.client.MongoCollection;
import com.mongodb.event.ServerHeartbeatFailedEvent;
import com.mongodb.event.ServerHeartbeatStartedEvent;
import com.mongodb.event.ServerHeartbeatSucceededEvent;
import com.mongodb.event.ServerMonitorListener;
import com.mycompany.wekamongo.database.MongoContext;
import com.mycompany.wekamongo.helpers.Alerts;
import com.mycompany.wekamongo.helpers.WindowCreator;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import org.bson.Document;


public class FXMLController implements Initializable, ServerMonitorListener {
    
    @FXML
    private TextField ipText, portText, databaseText;
    @FXML
    private ToggleButton connectButton;
    @FXML
    private Label statusLabel;
    @FXML
    private Label collectionNameLabel;
    @FXML
    private TextArea contentTextArea;
    @FXML
    private Button saveCollectionButton, loadCollectionButton;
    @FXML
    private MenuItem loadMenuItem, newMenuItem, importMenuItem, exportMenuItem, dropCollectionMenuItem;
    @FXML
    private HBox collectionHBox;
    
    private MongoContext db;
    private String nameActualCollection;
    private int actualRowsData;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        db = MongoContext.getInstance(this);
        actualRowsData = 0;
    }

    
    @FXML
    private void connectButtonAction(ActionEvent event) {
        if(connectButton.isSelected()){
            if(validationConnectionFields()){
                if(!db.connect(ipText.getText(), Integer.valueOf(portText.getText()), databaseText.getText())){
                    updateGUI(false, "błąd połączenia");
                } else {
                    updateGUI(true, "połączono");
                    if(!db.databaseExists(databaseText.getText())){
                        
                        if(!Alerts.conformation("Brak bazy", 
                            "Brak bazy o podanej nazwie. Czy utworzyć nową?", 
                            "Baza zostanie utworzona po dodaniu pierwszych obiektów."))
                        {
                            db.disconnect();
                            updateGUI(false, "rozłączono");
                        }
                    }
                }
            } else
                connectButton.setSelected(false);
        } else {
            db.disconnect();
            updateGUI(false, "rozłączono");
        }
    }
    
    @FXML
    private void loadMenuItemAction(ActionEvent event){
        String nameValue = collectionChooser();
        if(nameValue != null && !nameValue.isEmpty()){
           nameActualCollection = nameValue;
           setData(db.getCollection(nameActualCollection));
           exportMenuItem.setDisable(false);
        }
    }
    
    @FXML
    private void newMenuItemAction(ActionEvent event){
        WindowCreator<CollectionCreatorFXMLController> creator = new WindowCreator<>("CollectionCreatorFXML","Utwórz");
        CollectionCreatorFXMLController controller = creator.getController();
        creator.showAndWait();
        String nameNewCollection = controller.getNameCollection();
        if(nameNewCollection != null && !nameNewCollection.isEmpty()){
            exportMenuItem.setDisable(false);
            importMenuItem.setDisable(true);
            nameActualCollection = nameNewCollection;
            setData(db.getCollection(nameNewCollection));
        }
        
    }
    
    @FXML
    private void exportMenuItemAction(ActionEvent event){
        WindowCreator<ExportCollectionFXMLController> creator = new WindowCreator<>("ExportCollectionFXML", "Exportuj z pliku");
        ExportCollectionFXMLController controller = creator.getController();
        controller.setNameCollection(nameActualCollection);
        creator.showAndWait();
    }
    
    @FXML
    private void importMenuItemAction(ActionEvent event){
        WindowCreator<ImportCollectionFXMLController> creator = new WindowCreator<>("ImportCollectionFXML", "Importuj do pliku");
        creator.getController().setNameCollection(nameActualCollection);
        creator.showAndWait();
    }
    
    @FXML
    private void dropCollectionMenuItemAction(ActionEvent event){
        if(Alerts.conformation("Usuniecie kolekcji", "Czy napewno chcesz usunąć kolekcje "+nameActualCollection+"?", null)){
            db.dropCollection(nameActualCollection);
            contentTextArea.setText(null);
            exportMenuItem.setDisable(true);
            importMenuItem.setDisable(true);
            collectionNameLabel.setText(null);
            nameActualCollection = null;
            collectionHBox.setVisible(false);
        }
    }
    
    @FXML
    private void aboutMenuItemAction(ActionEvent event){
        WindowCreator<AboutFXMLController> creator = new WindowCreator<>("AboutFXML", "O programie");
        creator.showAndWait();
    }
    
    
    private boolean validationConnectionFields(){
        String  ipRegex = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$", 
                portRegex = "\\d+",
                databaseRegex = "[0-9a-zA-Z$_]+",
                ipString = ipText.getText(), 
                portString = portText.getText(),
                databaseString = databaseText.getText();
        boolean ipValid = false,
                portValid = false,
                databaseValid = false;
        
        if(!ipString.trim().isEmpty() || !portString.trim().isEmpty() || !databaseString.trim().isEmpty()){
            if(ipString.matches(ipRegex)){
                ipText.setStyle(null);
                ipValid = true;
            } else {
                ipText.setStyle("-fx-border-color: red");
            }
                
            if(portString.matches(portRegex)){
                portText.setStyle(null);
                portValid = true;
            } else {
                portText.setStyle("-fx-border-color: red");
            }
            
            if(databaseString.matches(databaseRegex)){
                databaseText.setStyle(null);
                databaseValid = true;
            } else {
                databaseText.setStyle("-fx-border-color: red");
            }
                
        } else {
            ipText.setStyle("-fx-border-color: red");
            portText.setStyle("-fx-border-color: red");
            databaseText.setStyle("-fx-border-color: red");
        }
        
        if(ipValid && portValid && databaseValid){
            return true;
        }else
            return false;
    }

    

    @Override
    public void serverHearbeatStarted(ServerHeartbeatStartedEvent event) {
        //System.out.println("Ping to database");
    }

    @Override
    public void serverHeartbeatSucceeded(ServerHeartbeatSucceededEvent event) {
        
        if(!db.isConnected()){
            db.setIsConnected(true);
            updateGUI(true, "połączono");
        } else if(db.getCollection()!=null && (actualRowsData!=db.getCollection().count())){    //when number of items changed
            System.out.println("update view..");
            setData(db.getCollection());                                                        //update view
        }

    }

    @Override
    public void serverHeartbeatFailed(ServerHeartbeatFailedEvent event) {
        updateGUI(false, "rozłączono");
    }
    
    
    
    private void updateGUI(final boolean isConnected, final String status){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if(isConnected){
                    connectButton.setText("Rozłącz");
                    ipText.setDisable(true);
                    portText.setDisable(true);
                    databaseText.setDisable(true);
                    connectButton.setSelected(true);
                    statusLabel.setText(status);
                    statusLabel.setTextFill(Color.GREEN);
                    loadMenuItem.setDisable(false);
                    newMenuItem.setDisable(false);
                } else {
                    connectButton.setText("Połącz");
                    ipText.setDisable(false);
                    portText.setDisable(false);
                    databaseText.setDisable(false);
                    connectButton.setSelected(false);
                    statusLabel.setText(status);
                    statusLabel.setTextFill(Color.DARKRED);
                    loadMenuItem.setDisable(true);
                    newMenuItem.setDisable(true);
                    contentTextArea.setText(null);
                    exportMenuItem.setDisable(true);
                    importMenuItem.setDisable(true);
                    dropCollectionMenuItem.setDisable(true);
                    collectionNameLabel.setText(null);
                    collectionHBox.setVisible(false);
                }
            }
        });
    }
    
    private String collectionChooser(){
        
        WindowCreator<CollectionChooserFXMLController> creator = new WindowCreator<CollectionChooserFXMLController>
                                                                                ("CollectionChooserFXML", "Kolekcje");
        creator.showAndWait();
        CollectionChooserFXMLController controller = creator.getController();
        if(controller != null){
            return controller.getCollectionName();
        } else
            statusLabel.setText("błąd wyświetlenia nowego okna");
        return null;
    }
    
    
    private synchronized void setData(MongoCollection<Document> collection){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                System.out.println("updating data..");
                List<Document> documents = (List<Document>) collection.find().into(new ArrayList<>());
                if(!documents.isEmpty()){
                    importMenuItem.setDisable(false);
                }
                collectionNameLabel.setText(nameActualCollection);
                contentTextArea.setText(null);
                collectionHBox.setVisible(true);
                dropCollectionMenuItem.setDisable(false);
                actualRowsData = 0;
                if(!documents.isEmpty()){
                    documents.stream().forEach((doc) -> {
                        String rowString = "";
                        Set<String> keys = doc.keySet();
                        for(String key : keys){
                            rowString += key+": "+doc.get(key).toString()+"\t";
                        }
                        contentTextArea.appendText(rowString+"\n");
                        actualRowsData++;
                    });
                }

            }
        });

    }
}
