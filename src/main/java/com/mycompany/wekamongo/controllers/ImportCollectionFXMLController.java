/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wekamongo.controllers;

import com.mongodb.client.MongoCollection;
import com.mycompany.wekamongo.database.MongoContext;
import com.mycompany.wekamongo.helpers.FileManagement;
import com.mycompany.wekamongo.helpers.WekaWrapper;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.bson.Document;
import weka.core.Instance;
import weka.core.Instances;

/**
 * FXML Controller class
 *
 * @author Kamil
 */
public class ImportCollectionFXMLController implements Initializable {

    @FXML
    private HBox attributeHBox;
    @FXML
    private Label nameCollectionLabel;
    @FXML
    private TableView<Document> contentTableView;
    private TableColumn<Document,String>[] columns;
    private ObservableList<Document> documentRows;
    
    
    private MongoContext db;
    private Instances instances;
    private MongoCollection<Document> collection;
    private List<Document> documents;
    private String nameCollection;
    private boolean isAttributeCompatibility;
    
    private final String[] attributeType = {"Tekstowy", "Numeryczny", "Symboliczny"};
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        db = MongoContext.getInstance(null);
        collection = db.getCollection();
        documents = (List<Document>) collection.find().into(new ArrayList<>());
        checkAttributeCompatibily();
        if(isAttributeCompatibility){
            Set<String> keys = collection.find().first().keySet();
            showTable(keys);
            createAttributes(keys);
            
        } else {
            new Alert(Alert.AlertType.ERROR, "Kolekcja zawieta niespójne dane!", ButtonType.CLOSE).show();
            ((Stage)nameCollectionLabel.getScene().getWindow()).close();
        }
    }    
    
    @FXML
    private void saveButtonAction(ActionEvent event){
        if(isAttributeCompatibility){
            List<String> attributestype = getChoosenAttributes();
            Instances instances = WekaWrapper.convertToInstances(documents, attributestype, nameCollection);
            if(instances != null){
                FileManagement.saveFile(instances);
                new Alert(Alert.AlertType.INFORMATION, "Zapis zakończony powodzeniem.", ButtonType.CLOSE).showAndWait();
                ((Stage)nameCollectionLabel.getScene().getWindow()).close();
            } else {
                new Alert(Alert.AlertType.ERROR, "Wystąpił błąd w konwersji danych", ButtonType.CLOSE).show();
            }
            
        }
    }

    public void setNameCollection(String nameCollection) {
        this.nameCollection = nameCollection;
        nameCollectionLabel.setText(nameCollection);
    }
    
    private List<String> getChoosenAttributes(){
        ObservableList<Node> itemsHBoxFXML = attributeHBox.getChildren();
        List<String> choosenTypes = new ArrayList<>();
        for(Node itemsVBoxFXML : itemsHBoxFXML){
            for(Node itemsFXML : ((VBox)itemsVBoxFXML).getChildren()){
                if(itemsFXML instanceof ComboBox){
                    choosenTypes.add((String)((ComboBox)itemsFXML).getValue());
                }
            }
        }
        return choosenTypes;
    }
    
    private boolean checkAttributeCompatibily(){
        Document firstDocument = collection.find().first();
        Set<String> keys = firstDocument.keySet();
        for(Document nextDocument : collection.find()){
            if(!keys.equals(nextDocument.keySet())){
                isAttributeCompatibility = false;
                return isAttributeCompatibility;
            }
        }
        isAttributeCompatibility = true;
        return isAttributeCompatibility;
    }
    
    //ATTRIBUTES
    private void createAttributes(Set<String> attributeNames){
        List<VBox> attributesFXML = new ArrayList<>();
        for(String name : attributeNames){
            attributesFXML.add(createAttribut(name));
            attributeHBox.getChildren().add(attributesFXML.get(attributesFXML.size()-1));
        }
    }
    
    private VBox createAttribut(String attributeName){
        VBox vbox = new VBox();
        vbox.getChildren().add(new Label(attributeName));
        ComboBox comboBox = new ComboBox(FXCollections.observableArrayList(Arrays.asList(attributeType)));
        comboBox.getSelectionModel().selectFirst();
        vbox.getChildren().add(comboBox);
        return vbox;
    }
    
    
    //TABLE
    private void showTable(Set<String> attributes){
        createTable(attributes);
        setItemsToTable();
    }
    
    private void createTable(Set<String> attribute){
        
        columns = new TableColumn[attribute.size()];
        int i=0;
        for(String nameAttr : attribute){
            columns[i] = new TableColumn<Document, String>(nameAttr);
            columns[i].setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().get(nameAttr).toString()));
            i++;
        }
        contentTableView.getColumns().addAll(columns);
    }
    
    private void setItemsToTable(){
        
        contentTableView.setItems(FXCollections.observableList(documents));
    }
    
}
