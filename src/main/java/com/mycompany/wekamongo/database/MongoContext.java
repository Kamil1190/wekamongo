/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wekamongo.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.event.ServerMonitorListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.bson.Document;


/**
 *
 * @author Kamil
 */
public class MongoContext{
    
    private MongoClient client;
    private MongoDatabase db;
    private MongoCollection<Document> collection;
    private MongoClientOptions clientOptions;
    private boolean isConnected=false;
    
    private static MongoContext singleton;
    
    public static MongoContext getInstance(ServerMonitorListener listener){
        if(singleton==null && listener!=null){
            singleton = new MongoContext(listener);
        }
        return singleton;
    }
    
    
    
    
    public MongoContext(ServerMonitorListener listener){
        clientOptions = MongoClientOptions.builder()
            .addServerMonitorListener(listener)
            .serverSelectionTimeout(5000)
            .build();
    }
    

    
    
    public boolean connect(String ip, int port, String database){
        client = new MongoClient(new ServerAddress(ip, port), clientOptions);
        db = client.getDatabase(database); 
        try{
            client.getAddress();
        } catch(Exception e){
            isConnected = false;
            disconnect();
            System.err.println("Connection error!");
        } finally{
            return isConnected;
        }
    }
    
    public void disconnect(){
        client.close();
        isConnected = false;
    }
    
    
    public boolean isConnected(){
        return isConnected;
    }
    
    public void setIsConnected(boolean value){
        isConnected = value;
    }
    
    
    public MongoIterable getCollectionNames(){
        return db.listCollectionNames(); 
    }
    
    public MongoCollection<Document> getCollection(String name){
        collection = db.getCollection(name);
        return collection; 
    }
    
    public MongoCollection<Document> getCollection(){
        return collection;
    }
    
    public void insertDocument(Document document){
        collection.insertOne(document);
    }
    
    public void insertDocuments(List<Document> documents){
        collection.insertMany(documents);
    }
    
    public void createCollection(String name){
        db.createCollection(name);
    }
    
    public void dropCollection(String name){
        db.getCollection(name).drop();
    }
    
    public boolean databaseExists(String databaseName){
        Set<String> collectionNames = client.listDatabaseNames().into(new HashSet<String>());;
        for (final String name : collectionNames) {
            if (name.equalsIgnoreCase(databaseName)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean collectionExists(final String collectionName) {
        Set<String> collectionNames = db.listCollectionNames().into(new HashSet<String>());;
        for (final String name : collectionNames) {
            if (name.equalsIgnoreCase(collectionName)) {
                return true;
            }
        }
        return false;
    }
}
