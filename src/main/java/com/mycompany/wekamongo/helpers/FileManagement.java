/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wekamongo.helpers;


import com.mycompany.wekamongo.models.FileDataModel;
import java.io.File;
import java.io.IOException;
import javafx.stage.FileChooser;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;

/**
 *
 * @author kamil
 */
public class FileManagement {
    
    
    
    // Odczytanie tablicy danych z dysku w formacie ARFF lub CSV
    public static FileDataModel loadData(File file) throws IOException {
        String path = file.getAbsolutePath(),
               extension;
        int i = path.lastIndexOf('.');
        
        if (i > 0) {
            extension = path.substring(i+1);
            System.out.println("extension: "+extension);

            if(extension.equals("arff")){
                ArffLoader loader = new ArffLoader();   // Utworzenie obiektu czytajacego dane z formatu ARFF
                loader.setFile(file);                   // Ustawienie pliku do odczytania
                return new FileDataModel(loader.getDataSet(), file.getName());            // Odczytanie danych z pliku
                
            } else if(extension.equals("csv")){
                CSVLoader loader = new CSVLoader();     // Utworzenie obiektu czytajacego dane z formatu CSV
                loader.setSource(file);                 // Ustawienie pliku do odczytania
                return new FileDataModel(loader.getDataSet(), file.getName());            // Odczytanie danych z pliku
            }

        }
        return null;
    }
    
    
    
    public static FileDataModel loadFromFile(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Otworz plik");
        fileChooser.setInitialDirectory(
            new File(System.getProperty("user.home"))
        );  
        System.out.println("Current working directory : " + System.getProperty("user.dir"));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Arff", "*.arff"),
                new FileChooser.ExtensionFilter("CSV", "*.csv")
        );
        File file = fileChooser.showOpenDialog(null);
        //System.out.println("File: "+file.getPath());
        if(file!=null){
            try {
                return loadData(file);
            } catch (IOException ex) {
                System.err.println("Błąd przy ładowaniu pliku! "+ex);
            }
        }
        return null;
    }
    
    public static void saveFile(Instances data){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Otworz plik");
        fileChooser.setInitialDirectory(
            new File(System.getProperty("user.home"))
        );  
        System.out.println("Current working directory : " + System.getProperty("user.dir"));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Arff", "*.arff"),
                new FileChooser.ExtensionFilter("CSV", "*.csv")
        );
        File file = fileChooser.showSaveDialog(null);
        
        String path = file.getAbsolutePath(), extension;
        System.out.println("path: "+path);
        int i = path.lastIndexOf('.');
        if (i > 0) {
            extension = path.substring(i+1);
            try{
                if(extension.equals("arff")){
                    saveArff(data, file);
                } else if(extension.equals("csv")){
                    saveCsv(data, file);
                }
            } catch (IOException ex) {
                System.err.println("Błąd przy zapisie pliku! "+ex);
            }
        }
    }
    
    // Zapis zbioru danych do formatu ARFF
    public static void saveArff(Instances data, File file) throws IOException {
        System.out.println("save arff");
        ArffSaver saver = new ArffSaver(); // Utworzenie obiektu zapisujacego dane
        saver.setFile(file); // Ustawienie nazwy pliku do zapisu
        saver.setInstances(data);
        saver.writeBatch();                // Zapis do pliku
    }
    
     // Zapis zbioru danych do formatu CSV
    public static void saveCsv(Instances data, File file) throws IOException{
        System.out.println("save csv");
        CSVSaver saver = new CSVSaver();
        saver.setFile(file); // Ustawienie nazwy pliku do zapisu
        saver.setInstances(data);
        saver.writeBatch();                // Zapis do pliku
    }
}
