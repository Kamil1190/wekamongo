/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wekamongo.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;
import org.bson.Document;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author Kamil
 */
public class WekaWrapper {
    
    public static Document convertToDocument(Instance instance){
        
        Document doc = new Document();
        for(int i=0;i<instance.numAttributes();i++){
            System.out.println(instance.attribute(i).name()+": "+instance.toString(i));
            doc.put(instance.attribute(i).name(), instance.toString(i));
        }
        return doc;   
    }
    
    
    public static List<Document> convertToDocuments(Instances instances){
        
        List<Document> documents = new ArrayList<Document>();
        
        for(int i=0; i<instances.numInstances();i++){
            Instance instance = instances.instance(i);
            Document doc = new Document();
            for(int j=0;j<instance.numAttributes();j++){
                doc.put(instance.attribute(j).name(), instance.toString(j));
            }
            documents.add(doc);
        }
        return documents;  
    }
    
    
    public static Instances convertToInstances(List<Document> documents, List<String> typeAttributes, String nameCollection){
        
        Instances instances = null;
        ArrayList<Attribute> attributes = new ArrayList<Attribute>();
        
        //CREATING ATTRIBUTES
        List<String> nameAttributes = new ArrayList<>();
        nameAttributes.addAll(documents.get(0).keySet());
        for(int i=0;i<nameAttributes.size();i++){
            String nameType = typeAttributes.get(i);
            String name = nameAttributes.get(i);
            Attribute attribute;
            switch(nameType){
                case "Tekstowy":
                    attribute = new Attribute(name, (ArrayList<String>)null);
                    break;
                case "Numeryczny":
                    attribute = new Attribute(name);
                    break;
                default: //"Symboliczny"
                    attribute = new Attribute(name, getDistinctValues(documents, name));
                    break;
            }
            attributes.add(attribute);
        }
        
        //CREATING INSTANCES
        instances = new Instances(nameCollection, attributes, 0);
        for(int i=0;i<documents.size();i++){
            
            Document document = documents.get(i);
            instances.add(new DenseInstance(attributes.size()));
            
            Instance instance = instances.get(instances.size()-1);
            int j=0;
            for(String keyAttribute : nameAttributes){
                if(typeAttributes.get(j).equals("Numeryczny")){
                    try{
                        instance.setValue(j, Double.valueOf(document.get(keyAttribute).toString()));
                    } catch(NumberFormatException ex){
                        return null;
                    } 
                } else {
                    instance.setValue(j, document.get(keyAttribute).toString());
                }
                j++;
            }
            
            
        }
        
        return instances;
    }
    
    private static ArrayList<String> getDistinctValues(List<Document> documents, String name){
        ArrayList<String> distinctValues = new ArrayList<>();
        
        for(Document document : documents){
            String actualValue = document.get(name).toString();
            if(!distinctValues.contains(actualValue)){
                distinctValues.add(actualValue);
            }
        }
        return distinctValues;
    }
}
