/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wekamongo.helpers;

import com.mycompany.wekamongo.controllers.CollectionChooserFXMLController;
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Kamil
 */
public class WindowCreator<T> {
      
    private T controller;
    private Stage stage;
    
    public WindowCreator(String FXMLfile, String title){
        try {
            FXMLLoader loader = new FXMLLoader();
            Parent root = loader.load(getClass().getResource("/fxml/"+FXMLfile+".fxml").openStream());
            controller = (T) loader.getController();
            Scene scene = new Scene(root);
            stage = new Stage();
            stage.setTitle(title);
            stage.setScene(scene);
        } catch (IOException ex) {
            System.err.println("Błąd tworzenia okna "+title+"\n"+ex);
        }
    }
    
    public void showAndWait(){
        stage.showAndWait(); 
    }
    
    public T getController(){
        return controller;
    }
    
            
}
