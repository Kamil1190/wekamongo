/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wekamongo.models;

import weka.core.Instances;

/**
 *
 * @author kamil
 */
public class FileDataModel{
    
    private Instances instances;
    private String filename;

    public FileDataModel() {
    }


    public FileDataModel(Instances instances, String filename) {
        this.instances = instances;
        this.filename = filename;
    }

    public Instances getInstances() {
        return instances;
    }

    public String getFilename() {
        return filename;
    }
        
}